/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-02-06 16:25:03
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-02-06 16:25:43
 * @FilePath: \src\api\he-run-sheng\v1\game\game.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */

import request from '@/utils/request'

// 获取
export function getGame(data)
{
  return request({
    url: `/game/getGame`,
    method: 'post',
    data
  })
}

// 添加
export function addGame(data)
{
  return request({
    url: `/game/addGame`,
    method: 'post',
    data
  })
}

// 更新
export function updateGame(data)
{
  return request({
    url: `/game/updateGame`,
    method: 'post',
    data
  })
}

// 删除
export function deleteGame(data)
{
  return request({
    url: `/game/deleteGame`,
    method: 'post',
    data
  })
}

// 批量删除
export function multipleDeleteGame(data)
{
  return request({
    url: `/game/multipleDeleteGame`,
    method: 'post',
    data
  })
}
