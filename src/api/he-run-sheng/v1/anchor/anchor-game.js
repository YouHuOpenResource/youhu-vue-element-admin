/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-01-23 08:26:01
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-23 08:33:11
 * @FilePath: \src\api\he-run-sheng\v1\anchor\anchor-game.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */

import request from '@/utils/request'

// 获取
export function getAnchorGame(data)
{
  return request({
    url: `/anchor/details/getAnchorGame`,
    method: 'post',
    data
  })
}

// 添加
export function addAnchorGame(data)
{
  return request({
    url: `/anchor/details/addAnchorGame`,
    method: 'post',
    data
  })
}

// 更新
export function updateAnchorGame(data)
{
  return request({
    url: `/anchor/details/updateAnchorGame`,
    method: 'post',
    data
  })
}

// 删除
export function deleteAnchorGame(data)
{
  return request({
    url: `/anchor/details/deleteAnchorGame`,
    method: 'post',
    data
  })
}

// 批量删除
export function multipleDeleteAnchorGame(data)
{
  return request({
    url: `/anchor/details/multipleDeleteAnchorGame`,
    method: 'post',
    data
  })
}
