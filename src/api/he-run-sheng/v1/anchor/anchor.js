
import request from '@/utils/request'

// 获取
export function getAnchor(data)
{
  return request({
    url: `/anchor/getAnchor`,
    method: 'post',
    data
  })
}

// 添加
export function addAnchor(data)
{
  return request({
    url: `/anchor/addAnchor`,
    method: 'post',
    data
  })
}

// 更新
export function updateAnchor(data)
{
  return request({
    url: `/anchor/updateAnchor`,
    method: 'post',
    data
  })
}

// 重新授权
export function resetAnchorAuth(data)
{
  return request({
    url: `/anchor/resetAnchorAuth`,
    method: 'post',
    data
  })
}

// 删除
export function deleteAnchor(data)
{
  return request({
    url: `/anchor/deleteAnchor`,
    method: 'post',
    data
  })
}

// 批量删除
export function multipleDeleteAnchor(data)
{
  return request({
    url: `/anchor/multipleDeleteAnchor`,
    method: 'post',
    data
  })
}
