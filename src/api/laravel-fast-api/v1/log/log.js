/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2022-03-16 10:12:55
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 17:17:42
 */
import request from '@/utils/request'

// 获取后台登录日志
export function getAdminLoginLog(data)
{
  return request({
    url: `/log/getAdminLoginLog`,
    method: 'post',
    data
  })
}

// 后台删除登录日志
export function deleteAdminLoginLog(data)
{
  return request({
    url: `/log/deleteAdminLoginLog`,
    method: 'post',
    data
  })
}

// 后台多选删除登录日志
export function multipleDeleteAdminLoginLog(data)
{
  return request({
    url: `/log/multipleDeleteAdminLoginLog`,
    method: 'post',
    data
  })
}

// 获取手机登录日志
export function getUserLoginLog(data)
{
  return request({
    url: `/log/getUserLoginLog`,
    method: 'post',
    data
  })
}

// 手机删除登录日志
export function deleteUserLoginLog(data)
{
  return request({
    url: `/log/deleteUserLoginLog`,
    method: 'post',
    data
  })
}

// 手机多选删除登录日志
export function multipleDeleteUserLoginLog(data)
{
  return request({
    url: `/log/multipleDeleteUserLoginLog`,
    method: 'post',
    data
  })
}

// 获取后台事件日志
export function getAdminEventLog(data)
{
  return request({
    url: `/log/getAdminEventLog`,
    method: 'post',
    data
  })
}

// 后台删除事件日志
export function deleteAdminEventLog(data)
{
  return request({
    url: `/log/deleteAdminEventLog`,
    method: 'post',
    data
  })
}
// 后台多选删除事件日志
export function multipleDeleteAdminEventLog(data)
{
  return request({
    url: `/log/multipleDeleteAdminEventLog`,
    method: 'post',
    data
  })
}

// 获取手机事件日志
export function getUserEventLog(data)
{
  return request({
    url: `/log/getUserEventLog`,
    method: 'post',
    data
  })
}

// 手机删除事件日志
export function deleteUserEventLog(data)
{
  return request({
    url: `/log/deleteUserEventLog`,
    method: 'post',
    data
  })
}
// 手机多选删除事件日志
export function multipleDeleteUserEventLog(data)
{
  return request({
    url: `/log/multipleDeleteUserEventLog`,
    method: 'post',
    data
  })
}
