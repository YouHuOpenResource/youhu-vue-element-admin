/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2024-12-19 15:27:02
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 17:10:25
 * @FilePath: \src\api\picture\album.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2021-10-28 14:48:53
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 17:06:01
 */
import request from '@/utils/request'

export function getAlbum(data)
{
  return request({
    url: `/picture/getAlbum`,
    method: 'post',
    data
  })
}

export function addAlbum(data)
{
  return request({
    url: `/picture/addAlbum`,
    method: 'post',
    data
  })
}

export function updateAlbum(data)
{
  return request({
    url: `/picture/updateAlbum`,
    method: 'post',
    data
  })
}

export function deleteAlbum(data)
{
  return request({
    url: `/picture/deleteAlbum`,
    method: 'post',
    data
  })
}

export function getAlbumPicture(data)
{
  return request({
    url: `/picture/getAlbumPicture`,
    method: 'post',
    data
  })
}

// 获取默认相册
export function getDefaultAlbum(data)
{
  return request({
    url: `/picture/getDefaultAlbum`,
    method: 'post',
    data
  })
}

// 查找相册
export function findAlbum(data)
{
  return request({
    url: `/picture/findAlbum`,
    method: 'post',
    data
  })
}
