import request from '@/utils/request'

// 头像上传
export function uploadAvatarPicture(data)
{
  return request({
    url: `/file/uploadAvatarPicture`,
    method: 'post',
    withCredentials: false,
    headers: { 'type': 'picture', 'Content-Type': 'multipart/form-data' },
    data
  })
}

// 单图上传
export function uploadSinglePicture(data)
{
  return request({
    url: `/file/uploadSinglePicture`,
    method: 'post',
    withCredentials: false,
    headers: { 'type': 'picture', 'Content-Type': 'multipart/form-data' },
    data
  })
}

// 多图上传
export function uploadMultiplePicture(data)
{
  return request({
    url: `/file/uploadMultiplePicture`,
    method: 'post',
    withCredentials: false,
    headers: { 'type': 'picture', 'Content-Type': 'multipart/form-data' },
    data
  })
}

// 单图替换上传
export function uploadResetPicture(data)
{
  return request({
    url: `/file/uploadResetPicture`,
    method: 'post',
    withCredentials: false,
    headers: { 'type': 'picture', 'Content-Type': 'multipart/form-data' },
    data
  })
}

// 单图替换上传
export function setCover(data)
{
  return request({
    url: `/picture/setCover`,
    method: 'post',
    data
  })
}

// 转移相册
export function moveAlbum(data)
{
  return request({
    url: `/picture/moveAlbum`,
    method: 'post',
    data
  })
}

// 批量转移相册
export function moveMultipleAlbum(data)
{
  return request({
    url: `/picture/moveMultipleAlbum`,
    method: 'post',
    data
  })
}

// 删除图片
export function deletePicture(data)
{
  return request({
    url: `/picture/deletePicture`,
    method: 'post',
    data
  })
}

// 批量删除图片
export function deleteMultiplePicture(data)
{
  return request({
    url: `/picture/deleteMultiplePicture`,
    method: 'post',
    data
  })
}

// 修改图片名称
export function updatePictureName(data)
{
  return request({
    url: `/picture/updatePictureName`,
    method: 'post',
    data
  })
}
