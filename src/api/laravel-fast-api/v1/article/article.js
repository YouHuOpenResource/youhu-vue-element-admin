/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2021-05-23 15:19:35
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-17 12:04:15
 */
import request from '@/utils/request'

// 获取文章
export function getArticle(data)
{
  return request({
    url: `/article/getArticle`,
    method: 'post',
    data
  })
}
// 添加文章
export function addArticle(data)
{
  return request({
    url: `/article/addArticle`,
    method: 'post',
    params: data
  })
}

// 更新文章
export function updateArticle(data)
{
  return request({
    url: `/article/updateArticle`,
    method: 'post',
    data
  })
}

// 删除文章
export function deleteArticle(data)
{
  return request({
    url: `/article/deleteArticle`,
    method: 'post',
    data
  })
}
// 删除文章
export function multipleDeleteArticle(data)
{
  return request({
    url: `/article/multipleDeleteArticle`,
    method: 'post',
    data
  })
}
// 文章置顶
export function toTopArticle(data)
{
  return request({
    url: `/article/toTopArticle`,
    method: 'post',
    data
  })
}

// 批量置顶
export function multipleToTopArticle(data)
{
  return request({
    url: `/article/multipleToTopArticle`,
    method: 'post',
    data
  })
}

// 批量取消置顶
export function multipleUnTopArticle(data)
{
  return request({
    url: `/article/multipleUnTopArticle`,
    method: 'post',
    data
  })
}

/**
 *
 *example 使用
 */
export function fetchList(query)
{
  return request({
    url: '/vue-element-admin/article/list',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id)
{
  return request({
    url: '/vue-element-admin/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv)
{
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data)
{
  return request({
    url: '/vue-element-admin/article/create',
    method: 'post',
    data
  })
}

/* export function updateArticle(data) {
  return request({
    url: '/vue-element-admin/article/update',
    method: 'post',
    data
  })
} */
