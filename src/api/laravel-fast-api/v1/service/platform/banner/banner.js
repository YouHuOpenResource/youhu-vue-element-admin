/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-01-16 15:04:05
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 15:07:16
 * @FilePath: \src\api\service\platform\banner\banner.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
import request from '@/utils/request'

export function getPhoneBanner(data)
{
  return request({
    url: `/platform/banner/getPhoneBanner`,
    method: 'post',
    data
  })
}

export function addPhoneBanner(data)
{
  return request({
    url: `/platform/banner/addPhoneBanner`,
    method: 'post',
    data
  })
}

export function updatePhoneBanner(data)
{
  return request({
    url: `/platform/banner/updatePhoneBanner`,
    method: 'post',
    data
  })
}

export function deletePhoneBanner(data)
{
  return request({
    url: `/platform/banner/deletePhoneBanner`,
    method: 'post',
    data
  })
}

// 修改轮播图图片
export function updatePhoneBannerPicture(data)
{
  return request({
    url: `/platform/banner/updatePhoneBannerPicture`,
    method: 'post',
    data
  })
}

// 修改轮播图跳转
export function updatePhoneBannerUrl(data)
{
  return request({
    url: `/platform/banner/updatePhoneBannerUrl`,
    method: 'post',
    data
  })
}

// 修改轮播图排序
export function updatePhoneBannerSort(data)
{
  return request({
    url: `/platform/banner/updatePhoneBannerSort`,
    method: 'post',
    data
  })
}

// 修改轮播图备注
export function updatePhoneBannerRemarkInfo(data)
{
  return request({
    url: `/platform/banner/updatePhoneBannerRemarkInfo`,
    method: 'post',
    data
  })
}
