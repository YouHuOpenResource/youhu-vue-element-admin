/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2021-10-12 08:59:59
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 15:29:09
 */
import request from '@/utils/request'

export function getRegions()
{
  return request({
    url: `/region/getRegions`,
    method: 'get'
  })
}

export function getTreeRegions()
{
  return request({
    url: `/region/getTreeRegions`,
    method: 'get'
  })
}

export function addRegion(data)
{
  return request({
    url: `/region/addRegion`,
    method: 'post',
    data
  })
}

export function updateRegion(data)
{
  return request({
    url: `/region/updateRegion`,
    method: 'post',
    data
  })
}

export function moveRegion(data)
{
  return request({
    url: `/region/moveRegion`,
    method: 'post',
    data
  })
}

export function deleteRegion(data)
{
  return request({
    url: `/region/deleteRegion`,
    method: 'post',
    data
  })
}
