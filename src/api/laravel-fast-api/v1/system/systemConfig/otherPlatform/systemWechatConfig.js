/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2022-04-07 11:55:36
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 15:23:32
 */
import request from '@/utils/request'

// 获取
export function getSystemWechatConfig(data)
{
  return request({
    url: '/system/other-platform/getSystemWechatConfig',
    method: 'post',
    data
  })
}

// 添加
export function addSystemWechatConfig(data)
{
  return request({
    url: '/system/other-platform/addSystemWechatConfig',
    method: 'post',
    data
  })
}

// 更新
export function updateSystemWechatConfig(data)
{
  return request({
    url: '/system/other-platform/updateSystemWechatConfig',
    method: 'post',
    data
  })
}

// 删除
export function deleteSystemWechatConfig(data)
{
  return request({
    url: '/system/other-platform/deleteSystemWechatConfig',
    method: 'post',
    data
  })
}

// 批量删除
export function multipleDeleteSystemWechatConfig(data)
{
  return request({
    url: '/system/other-platform/multipleDeleteSystemWechatConfig',
    method: 'post',
    data
  })
}
