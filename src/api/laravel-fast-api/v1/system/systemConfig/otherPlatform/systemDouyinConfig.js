/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2022-04-07 11:55:36
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2024-12-31 18:40:04
 */
import request from '@/utils/request'

// 获取
export function getSystemDouyinConfig(data)
{
  return request({
    url: '/system/other-platform/getSystemDouyinConfig',
    method: 'post',
    data
  })
}

// 添加
export function addSystemDouyinConfig(data)
{
  return request({
    url: '/system/other-platform/addSystemDouyinConfig',
    method: 'post',
    data
  })
}

// 更新
export function updateSystemDouyinConfig(data)
{
  return request({
    url: '/system/other-platform/updateSystemDouyinConfig',
    method: 'post',
    data
  })
}

// 删除
export function deleteSystemDouyinConfig(data)
{
  return request({
    url: '/system/other-platform/deleteSystemDouyinConfig',
    method: 'post',
    data
  })
}

// 批量删除
export function multipleDeleteSystemDouyinConfig(data)
{
  return request({
    url: '/system/other-platform/multipleDeleteSystemDouyinConfig',
    method: 'post',
    data
  })
}
