/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2022-04-07 11:55:36
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 15:23:59
 */
import request from '@/utils/request'

// 获取
export function getVoiceConfig(data)
{
  return request({
    url: `/system/voice/getVoiceConfig`,
    method: 'post',
    data
  })
}

// 添加
export function addVoiceConfig(data)
{
  return request({
    url: `/system/voice/addVoiceConfig`,
    method: 'post',
    data
  })
}

// 更新
export function updateVoiceConfig(data)
{
  return request({
    url: `/system/voice/updateVoiceConfig`,
    method: 'post',
    data
  })
}

// 删除
export function deleteVoiceConfig(data)
{
  return request({
    url: `/system/voice/deleteVoiceConfig`,
    method: 'post',
    data
  })
}

// 批量删除
export function multipleDeleteVoiceConfig(data)
{
  return request({
    url: `/system/voice/multipleDeleteVoiceConfig`,
    method: 'post',
    data
  })
}
