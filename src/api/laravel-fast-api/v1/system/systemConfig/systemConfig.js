/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2022-04-07 11:55:36
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 15:16:11
 */
import request from '@/utils/request'

// 查询系统配置
export function getSystemConfig(data)
{
  return request({
    url: `/system/config/getSystemConfig`,
    method: 'post',
    data
  })
}

// 添加系统配置
export function addSystemConfig(data)
{
  return request({
    url: `/system/config/addSystemConfig`,
    method: 'post',
    data
  })
}

// 修改配置项
export function updateSystemConfig(data)
{
  return request({
    url: `/system/config/updateSystemConfig`,
    method: 'post',
    data
  })
}

// 删除配置项
export function deleteSystemConfig(data)
{
  return request({
    url: `/system/config/deleteSystemConfig`,
    method: 'post',
    data
  })
}

// 批量删除配置项
export function multipleDeleteSystemConfig(data)
{
  return request({
    url: `/system/config/multipleDeleteSystemConfig`,
    method: 'post',
    data
  })
}
