/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2024-12-19 15:27:02
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 14:42:30
 * @FilePath: \src\api\system\upload\upload.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
import request from '@/utils/request'

/**
 * 文件山传
 */

// 后台上传配置文件
export function uploadConfigFile(data)
{
  return request({
    url: `/file/uploadConfigFile`,
    method: 'post',
    withCredentials: false,
    data
  })
}

