/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-01-16 14:38:33
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 14:42:07
 * @FilePath: \src\api\system\upload\picture.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
import request from '@/utils/request'

/**
 * 图片上传
 */

// 修改头像
export function uploadUserAvatar(data)
{
  return request({
    url: `file/uploadUserAvatar`,
    method: 'post',
    withCredentials: false,
    data
  })
}

// 替换上传
export function uploadResetPicture(data)
{
  return request({
    url: `file/uploadResetPicture`,
    method: 'post',
    withCredentials: false,
    data
  })
}

// 单图上传
export function uploadSinglePicture(data)
{
  return request({
    url: `file/uploadSinglePicture`,
    method: 'post',
    withCredentials: false,
    data
  })
}

// 多图上传
export function uploadMultiplePicture(data)
{
  return request({
    url: `file/uploadMultiplePicture`,
    method: 'post',
    withCredentials: false,
    data
  })
}
