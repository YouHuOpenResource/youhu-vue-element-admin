/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2022-04-21 10:58:18
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 15:35:21
 */
import request from '@/utils/request'

// 上传
export function uploadExcel(data)
{
  return request({
    url: `/file/uploadExcel`,
    method: 'post',
    data
  })
}
// 下载
export function downloadBank(data)
{
  return request({
    url: `/file/downloadBank`,
    method: 'post',
    data
  })
}

// 获取默认银行
export function defaultBank(params)
{
  return request({
    url: `/bank/defaultBank`,
    method: 'get',
    params
  })
}

// 获取默认银行
export function findBank(data)
{
  return request({
    url: `/bank/findBank`,
    method: 'post',
    data
  })
}

// 获取
export function getBank(data)
{
  return request({
    url: `/bank/getBank`,
    method: 'post',
    data
  })
}

// 添加
export function addBank(data)
{
  return request({
    url: `/bank/addBank`,
    method: 'post',
    data
  })
}

// 更新
export function updateBank(data)
{
  return request({
    url: `/bank/updateBank`,
    method: 'post',
    data
  })
}

// 删除用户
export function deleteBank(data)
{
  return request({
    url: `/bank/deleteBank`,
    method: 'post',
    data
  })
}

// 批量禁用用户
export function multipleDisableBank(data)
{
  return request({
    url: `/bank/multipleDisableBank`,
    method: 'post',
    data
  })
}

// 批量删除用户
export function multipleDeleteBank(data)
{
  return request({
    url: `/bank/multipleDeleteBank`,
    method: 'post',
    data
  })
}
