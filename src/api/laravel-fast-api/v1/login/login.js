import request from '@/utils/request'

// 登录
export function login(data)
{
  return request({
    url: `/login`,
    method: 'post',
    data
  })
}
// 退出
export function logout()
{
  return request({
    url: `/logout`,
    method: 'get'
  })
}

// 获取登录管理员信息
export function getAdminInfo(token)
{
  return request({
    url: `/getAdminInfo`,
    method: 'get',
    params: { token }
  })
}

// 获取路由
export function getTreePermission()
{
  return request({
    url: `/getTreePermission`,
    method: 'get'
  })
}

// 获取所有提示配置
export function getAllSystemVoiceConfig()
{
  return request({
    url: `/getAllSystemVoiceConfig`,
    method: 'get'
  })
}

