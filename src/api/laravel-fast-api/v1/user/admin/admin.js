/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2021-05-23 15:19:35
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 16:02:03
 */

import request from '@/utils/request'

/**
 * 获取管理员用户信息 作为选项使用
 * @returns
 */
export function getDefaultAdmin()
{
  return request({
    url: `/admin/getDefaultAdmin`,
    method: 'get'
  })
}

export function findAdmin(data)
{
  return request({
    url: `/admin/findAdmin`,
    method: 'post',
    data
  })
}
// 获取用户
export function getAdmin(data)
{
  return request({
    url: `/admin/getAdmin`,
    method: 'post',
    data
  })
}

// 添加用户
export function addAdmin(data)
{
  return request({
    url: `/admin/addAdmin`,
    method: 'post',
    data
  })
}

// 更新用户
export function updateAdmin(data)
{
  return request({
    url: `/admin/updateAdmin`,
    method: 'post',
    data
  })
}

// 禁用用户
export function disableAdmin(data)
{
  return request({
    url: `/admin/disableAdmin`,
    method: 'post',
    data
  })
}

// 删除用户
export function deleteAdmin(data)
{
  return request({
    url: `/admin/deleteAdmin`,
    method: 'post',
    data
  })
}

// 批量禁用用户
export function multipleDisableAdmin(data)
{
  return request({
    url: `/admin/multipleDisableAdmin`,
    method: 'post',
    data
  })
}

// 批量删除用户
export function multipleDeleteAdmin(data)
{
  return request({
    url: `/admin/multipleDeleteAdmin`,
    method: 'post',
    data
  })
}
