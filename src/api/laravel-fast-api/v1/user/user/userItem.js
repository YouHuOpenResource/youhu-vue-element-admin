/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-01-16 16:53:36
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 16:54:30
 * @FilePath: \src\api\user\user\userItem.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
import request from '@/utils/request'

// 获取默认选项用户
export function getDefaultUser(params)
{
  console.log(params)
  return request({
    url: `/user/user-item/getDefaultUser`,
    method: 'get',
    params
  })
}

// 查找用户
export function findUser(data)
{
  return request({
    url: `/user/user-item/findUser`,
    method: 'post',
    data
  })
}
