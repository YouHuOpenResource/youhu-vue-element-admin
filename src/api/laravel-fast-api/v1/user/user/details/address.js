/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-01-16 16:17:55
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 16:18:48
 * @FilePath: \src\api\user\user\details\address.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
import request from '@/utils/request'

// 获取用户地址
export function getUserAddress(params)
{
  return request({
    url: `/user/address/getUserAddress`,
    method: 'get',
    params
  })
}

// 添加用户地址
export function addUserAddress(data)
{
  return request({
    url: `/user/address/addUserAddress`,
    method: 'post',
    data
  })
}

// 删除用户地址
export function deleteUserAddress(params)
{
  return request({
    url: `/user/address/deleteUserAddress`,
    method: 'get',
    params
  })
}

// 设置用户默认地址
export function setDefaultUserAddress(params)
{
  return request({
    url: `/user/address/setDefaultUserAddress`,
    method: 'get',
    params
  })
}
