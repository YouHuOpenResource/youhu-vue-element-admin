/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-01-16 16:06:44
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 16:50:37
 * @FilePath: \src\api\user\user\details\details.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
import request from '@/utils/request'

// 修改头像
export function uploadUserAvatar(data)
{
  return request({
    url: `/file/uploadUserAvatar`,
    method: 'post',
    data
  })
}

// 获取用户二维码
export function getUserQrcode(data)
{
  return request({
    url: `/user/details/getUserQrcode`,
    method: 'post',
    data
  })
}

// 更改用户级别
export function changeUserLevel(data)
{
  return request({
    url: `/user/details/changeUserLevel`,
    method: 'post',
    data
  })
}
// 修改用户手机号
export function updateUserPhone(data)
{
  return request({
    url: `/user/details/updateUserPhone`,
    method: 'post',
    data
  })
}

// 修改用户真实姓名
export function updateUserRealName(data)
{
  return request({
    url: `/user/details/updateUserRealName`,
    method: 'post',
    data
  })
}

// 修改用户昵称
export function updateUserNickName(data)
{
  return request({
    url: `/user/details/updateUserNickName`,
    method: 'post',
    data
  })
}

// 修改用户性别
export function updateUserSex(data)
{
  return request({
    url: `/user/details/updateUserSex`,
    method: 'post',
    data
  })
}

// 修改用户生日
export function updateUserBirthdayTime(data)
{
  return request({
    url: `/user/details/updateUserBirthdayTime`,
    method: 'post',
    data
  })
}

// 修改用户密码
export function resetUserPassword(data)
{
  return request({
    url: `/user/details/resetUserPassword`,
    method: 'post',
    data
  })
}
