import request from '@/utils/request'

// 获取用户身份证
export function getUserIdCard(data)
{
  return request({
    url: `/user/real-auth/getUserIdCard`,
    method: 'post',
    data
  })
}

// 设置用户身份证
export function setUserIdCard(data)
{
  return request({
    url: `/user/real-auth/setUserIdCard`,
    method: 'post',
    data
  })
}

// 修改用身份证号
export function updateUserIdNumber(data)
{
  return request({
    url: `/user/real-auth/updateUserIdNumber`,
    method: 'post',
    data
  })
}
