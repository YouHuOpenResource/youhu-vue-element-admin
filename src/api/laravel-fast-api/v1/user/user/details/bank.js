import request from '@/utils/request'

// 获取用户银行卡
export function getUserBank(data)
{
  return request({
    url: '/getUserBank',
    method: 'post',
    data
  })
}

// 添加银行卡
export function addUserBank(data)
{
  return request({
    url: `/user/bank/addUserBank`,
    method: 'post',
    data
  })
}

// 添加银行卡
export function setDefaultBank(data)
{
  return request({
    url: `/user/bank/setUserDefaultBank`,
    method: 'post',
    data
  })
}

// 添加银行卡
export function deleteBank(data)
{
  return request({
    url: `/user/bank/deleteUserBank`,
    method: 'post',
    data
  })
}

