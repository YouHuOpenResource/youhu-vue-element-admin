/*
 * @Descripttion:
 * @version:
 * @Author: YouHuJun
 * @Date: 2021-05-23 15:19:35
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 16:17:25
 */
import request from '@/utils/request'

// 获取用户的上级用户(推荐用户)
export function getUserSource(data)
{
  return request({
    url: `/user/team/getUserSource`,
    method: 'post',
    data
  })
}

// 获取用户下级团队
export function getUserLowerTeam(data)
{
  return request({
    url: `/user/team/getUserLowerTeam`,
    method: 'post',
    data
  })
}

