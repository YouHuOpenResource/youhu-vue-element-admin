/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-01-16 16:04:44
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 16:06:08
 * @FilePath: \src\api\user\user\details\account.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
import request from '@/utils/request'

// 获取用户账户日志
export function getUserAccountLog(data)
{
  return request({
    url: `user/account/getUserAccountLog`,
    method: 'post',
    data
  })
}

// 设置用户账户
export function setUserAccount(data)
{
  return request({
    url: `user/account/setUserAccount`,
    method: 'post',
    data
  })
}
