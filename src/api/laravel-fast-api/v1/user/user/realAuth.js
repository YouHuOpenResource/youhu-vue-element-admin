/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2025-01-16 16:30:45
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 16:36:53
 * @FilePath: \src\api\user\user\details\realAuth.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */
import request from '@/utils/request'

// 获取用户实名认证申请
export function getUserRealAuthApply(data)
{
  return request({
    url: `/user/real-auth/getUserRealAuthApply`,
    method: 'post',
    data
  })
}

// 审核实名认证用户
export function realAuthUser(data)
{
  return request({
    url: `/user/real-auth/realAuthUser`,
    method: 'post',
    data
  })
}
