/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2024-12-19 15:27:02
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-01-16 16:39:33
 * @FilePath: \src\api\user\user\user.js
 * Copyright (C) 2025 youhujun. All rights reserved.
 */

import request from '@/utils/request'

// 获取用户
export function getUser(data)
{
  return request({
    url: `/user/getUser`,
    method: 'post',
    data
  })
}

// 添加用户
export function addUser(data)
{
  return request({
    url: `/user/addUser`,
    method: 'post',
    data
  })
}

// 更新用户
export function updateUser(data)
{
  return request({
    url: `/user/updateUser`,
    method: 'post',
    data
  })
}

// 禁用用户
export function disableUser(data)
{
  return request({
    url: `/user/disableUser`,
    method: 'post',
    data
  })
}

// 删除用户
export function deleteUser(data)
{
  return request({
    url: `/user/deleteUser`,
    method: 'post',
    data
  })
}

// 批量禁用用户
export function multipleDisableUser(data)
{
  return request({
    url: `/user/multipleDisableUser`,
    method: 'post',
    data
  })
}

// 批量删除用户
export function multipleDeleteUser(data)
{
  return request({
    url: `/user/multipleDeleteUser`,
    method: 'post',
    data
  })
}

