/*
 * @Descripttion:
 * @version: v1
 * @Author: youhujun 2900976495@qq.com
 * @Date: 2024-01-08 10:06:01
 * @LastEditors: youhujun 2900976495@qq.com
 * @LastEditTime: 2025-02-06 16:19:33
 * @FilePath: \src\router\map-router\herunsheng.js
 */

import Layout from '@/layout'

export const mapRouter = {
  'Layout': Layout,
  // 系统设置
  //    菜单管理
  'permission': () => import('@/pages/laravel-fast-api/v1/system/permission/index'),
  //    平台配置
  'platformConfig': () => import('@/pages/laravel-fast-api/v1/system/platform/index'),
  //        缓存设置
  'cacheConfig': () => import('@/pages/laravel-fast-api/v1/system/platform/cacheConfig/index'),
  //    系统配置
  'system': () => import('@/pages/laravel-fast-api/v1/system/system/index'),
  //		参数配置
  'systemConfig': () => import('@/pages/laravel-fast-api/v1/system/system/systemConfig/index'),
  //		提示配置
  'voiceConfig': () => import('@/pages/laravel-fast-api/v1/system/system/voiceConfig/index'),
  //		三方平台
  'otherPaltform': () => import('@/pages/laravel-fast-api/v1/system/system/otherPaltform/index'),
  //			微信平台
  'wechatPlatform': () => import('@/pages/laravel-fast-api/v1/system/system/otherPaltform/wechatPlatform/index'),
  //			抖音平台
  'douyinPlatform': () => import('@/pages/laravel-fast-api/v1/system/system/otherPaltform/douyinPlatform/index'),
  //    角色管理
  'role': () => import('@/pages/laravel-fast-api/v1/system/role/index'),
  //    地区管理
  'region': () => import('@/pages/laravel-fast-api/v1/system/region/index'),
  //    银行管理
  'bank': () => import('@/pages/laravel-fast-api/v1/system/bank/index'),

  // 业务设置
  //	集合设置
  //     分类管理
  'group': () => import('@/pages/laravel-fast-api/v1/service/group/index'),
  //        产品分类
  'goodsClass': () => import('@/pages/laravel-fast-api/v1/service/group/goodsClass/index'),
  //        文章分类
  'category': () => import('@/pages/laravel-fast-api/v1/service/group/category/index'),
  //        标签管理
  'label': () => import('@/pages/laravel-fast-api/v1/service/group/label/index'),
  //    级别管理
  'level': () => import('@/pages/laravel-fast-api/v1/service/level/index'),
  //        级别条件
  'levelItem': () => import('@/pages/laravel-fast-api/v1/service/level/levelItem/index'),
  //        用户级别
  'userLevel': () => import('@/pages/laravel-fast-api/v1/service/level/userLevel/index'),
  //    业务平台
  'servicePlatform': () => import('@/pages/laravel-fast-api/v1/service/platform/index'),
  //        首页轮播
  'phoneIndexBanner': () => import('@/pages/laravel-fast-api/v1/service/platform/phoneIndexBanner/index'),

  // 用户管理
  //    管理员管理
  'admin': () => import('@/pages/laravel-fast-api/v1/user/admin/admin'),
  //    用户管理
  'user': () => import('@/pages/laravel-fast-api/v1/user/user/index'),
  //        等待认证
  'userCheck': () => import('@/pages/laravel-fast-api/v1/user/user/userCheck/index'),
  //        用户列表
  'userList': () => import('@/pages/laravel-fast-api/v1/user/user/userList/user'),
  //        编辑用户
  'editUser': () => import('@/pages/laravel-fast-api/v1/user/user/editUser/index'),

  //    主播管理
  'anchor': () => import('@/pages/he-run-sheng/v1/anchor/index.vue'),
  //        用户列表
  'anchorList': () => import('@/pages/he-run-sheng/v1/anchor/anchorList/index.vue'),
  //        编辑用户
  'anchorEdit': () => import('@/pages/he-run-sheng/v1/anchor/anchorEdit/index.vue'),

  // 游戏管理
  //		游戏列表
  'gameList': () => import('@/pages/he-run-sheng/v1/game/gameList/index.vue'),
  //		编辑游戏
  'gameEdit': () => import('@/pages/he-run-sheng/v1/game/gameEdit/index.vue'),

  // 财务管理
  //    提现管理
  'withdraw': () => import('@/pages/laravel-fast-api/v1/financial/withdraw/index'),

  // 文章管理
  //    公告管理
  'notice': () => import('@/pages/laravel-fast-api/v1/article/notice/notice'),
  //    系统文章
  'systemArticle': () => import('@/pages/laravel-fast-api/v1/article/systemArticle/index'),

  // 图片空间
  //    我的相册
  'album': () => import('@/pages/laravel-fast-api/v1/picture/album/album'),

  // 日志管理
  //    登录日志
  'loginLog': () => import('@/pages/laravel-fast-api/v1/log/login/index'),
  //        手机登录日志
  'phoneLogin': () => import('@/pages/laravel-fast-api/v1/log/login/phoneLogin/index'),
  //        后台登录日志
  'adminLogin': () => import('@/pages/laravel-fast-api/v1/log/login/adminLogin/index'),
  //    事件日志
  'eventLog': () => import('@/pages/laravel-fast-api/v1/log/event/index'),
  //        手机事件日志
  'phoneEvent': () => import('@/pages/laravel-fast-api/v1/log/event/phoneEvent/index'),
  //        后台事件日志
  'adminEvent': () => import('@/pages/laravel-fast-api/v1/log/event/adminEvent/index')

  // 建议反馈

  // 消息通知

}
